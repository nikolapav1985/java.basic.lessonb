Basic lesson B
--------------

- StringADT.java (ADT example, check comments for details)
- TestStringADT.java (ADT example test, check comments for details)

Topic covered (ADT abstract data type)
--------------------------------------

- ADT (data and specification of operations on data, mainly related to making additional elements of a class)
- A library class (ADT as data and operations, no executable program, this is used later)
- Data abstraction (things you can do to a collection of data, postpone exact steps and details)

Practice (this lesson)
----------------------

- basically read multiple string lines from file and print
- any loop can be used
- line one (first line of file) contains an integer (this is number of text lines in a file coming next)
- next file contains multiple text lines (count n), read these lines and print

Practice (example file)
-----------------------

    3
    bbbbbbbbbb
    cccccccccc
    aaaaaaaaaa

Practice (example solution)
---------------------------

    import java.io.*;
    import java.util.*;

    public class BasicFileLines {
        public static void main(String args[])throws FileNotFoundException
        {
            // read multiple lines from file and print
            int i,count;
            String line,filename;
            Scanner in;

            if(args.length != 1){ // need one argument
                System.exit(1);
            }

            filename=args[0];
            in=new Scanner(new File(filename));
            count=Integer.parseInt(in.nextLine());
            for(i=0;i<count;i++){
                line=in.nextLine();
                System.out.println(line);
            }
        }
    }

