/**
*
* public class TestStringADT
*
*/
public class TestStringADT{
    public static void main(String args[]){
        StringADT init;
        StringADT other;
        StringADT prefix;
        StringADT suffix;
        StringADT capitals;
        StringADT los;

        init=new StringADT(args[0]);
        other=new StringADT(args[1]);
        prefix=init.prefix(other.getSelf());
        suffix=init.suffix(other.getSelf());
        capitals=init.capitals();
        los=init.los();

        System.out.println(init.getSelf());
        System.out.println(other.getSelf());
        System.out.println(prefix.getSelf());
        System.out.println(suffix.getSelf());
        System.out.println(capitals.getSelf());
        System.out.println(los.getSelf());
    }
}
