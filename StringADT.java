/**
*
* public class StringADT
*
* ADT abstract data type for string IMMUTABLE
*
*/
public class StringADT{
    public String self;
    public StringADT(String init){ 
        this.self=init;
    }
    public StringADT(char[] init){ // TODO
    }
    public String getSelf(){ // get self as string
        return this.self;
    }
    public StringADT prefix(String other){ // prefix to self, producer
        return new StringADT(other.concat(this.self));
    }
    public StringADT suffix(String other){ // suffix to self, producer
        return new StringADT(this.self.concat(other));
    }
    public StringADT capitals(){ // all capitals self, producer
        return new StringADT(this.self.toUpperCase());
    }
    public StringADT los(){ // all los self, producer
        return new StringADT(this.self.toLowerCase());
    }
    public char charAt(int index){ // get character at position, observer
        return this.self.charAt(index);
    }
}
